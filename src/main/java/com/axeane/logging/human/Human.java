package com.axeane.logging.human;

public abstract class Human {
  String name;
  int age;

  public Human(String name, int age) {
    this.name = name;
    this.age = age;
  }
}
