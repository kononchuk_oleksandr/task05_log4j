package com.axeane.logging.human;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Woman extends Human {
  private static Logger logger = LogManager.getLogger(Woman.class);

  public Woman(String name, int age) {
    super(name, age);
    logger.trace(this.toString());
    logger.debug(this.toString());
    logger.info(this.toString());
    logger.warn(this.toString());
    logger.error(this.toString());
    logger.fatal(this.toString());
  }

  @Override
  public String toString() {
    return "Object Woman" +
        " with name " + name +
        " and age " + age +
        " was added.";
  }
}
