package com.axeane.logging;

import com.axeane.logging.animals.Animal;
import com.axeane.logging.animals.Cat;
import com.axeane.logging.animals.Dog;
import com.axeane.logging.human.Human;
import com.axeane.logging.human.Men;
import com.axeane.logging.human.Woman;
import org.apache.logging.log4j.*;

public class Application {
  private static Logger logger = LogManager.getLogger(Application.class);

  public static void main(String[] args) {
    logger.trace("This is the trace message");
    logger.debug("This is the debug message");
    logger.info("This is an info message");
    logger.warn("This is a warn message");
    logger.error("This is an error message");
    logger.fatal("This is a fatal message");

    Animal cat = new Cat("Barsik", 5);
    Animal dog = new Dog("Sharik", 7);
    Human men = new Men("Alex", 28);
    Human woman = new Woman("Ann", 26);
  }

}
