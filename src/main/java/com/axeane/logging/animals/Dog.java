package com.axeane.logging.animals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dog extends Animal{
  private static Logger logger = LogManager.getLogger(Dog.class);

  public Dog(String name, int age) {
    super(name, age);
    logger.trace(this.toString());
    logger.debug(this.toString());
    logger.info(this.toString());
    logger.warn(this.toString());
    logger.error(this.toString());
    logger.fatal(this.toString());
  }

  @Override
  public String toString() {
    return "Object Dog" +
        " with name " + name +
        " and age " + age +
        " was added.";
  }
}
