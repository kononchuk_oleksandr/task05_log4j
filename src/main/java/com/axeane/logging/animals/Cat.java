package com.axeane.logging.animals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cat extends Animal {
  private static Logger logger = LogManager.getLogger(Cat.class);

  public Cat(String name, int age) {
    super(name, age);
    logger.trace(this.toString());
    logger.debug(this.toString());
    logger.info(this.toString());
    logger.warn(this.toString());
    logger.error(this.toString());
    logger.fatal(this.toString());
  }

  @Override
  public String toString() {
    return "Object Cat" +
        " with name " + name +
        " and age " + age +
        " was added.";
  }
}
